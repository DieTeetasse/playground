package playground;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import javax.swing.JFrame;

import SimpleOpenNI.SimpleOpenNI;

import processing.core.PApplet;
import processing.core.PVector;

public class Playground extends PApplet {
	private static final long serialVersionUID = 1L;
	
	static final String NAME = "Playground v0.4";
	static final boolean PLAY_WITH_MOUSE = true;
	
	static final int HEIGHT = 800;
	static final int WIDTH = 1280;
	
	static final int SPAWN_RAD = 30;
	static final int PLAYER_RAD = 50;
	static final int COLLECTOR_RAD = 45;
	
	static final int PARTICLES = 1500;
	static final int WIN_PARTICLES = 100;
	
	static final float FRONT_X = 550; //550 [600]
	static final float FRONT_Z = 1450; //1450 [2200]
	static final float FRONT_DIST = FRONT_X * 2;
	
	static final float BACK_X = 650;	//650 [900]
	static final float BACK_Z = 3450;
	static final float BACK_DIST = BACK_X * 2;
	
	static final float FRONT_BACK_DIST = BACK_Z - FRONT_Z;
	static final float FRONT_BACK_DIST_X = BACK_X - FRONT_X;
	
	List<Integer> user = new ArrayList<Integer>();
	Output o;
	SimpleOpenNI context;
	Level lvl; 
	boolean win = false;
	boolean pressed = false;
	boolean pressed2 = false;
	
	public void setup() {
		size(WIDTH, HEIGHT, P3D);
		
		if (!PLAY_WITH_MOUSE) {
			context = new SimpleOpenNI(this, SimpleOpenNI.RUN_MODE_MULTI_THREADED);
			context.enableDepth();
			context.enableRGB();
			context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
		
//			o = new Output();
		}
		
		background(0, 0, 0);
		stroke(0, 0, 255);
		strokeWeight(3);
		smooth();
		orientation(LANDSCAPE);
		
		lvl = Level.getNextLevel(this);
	}

	public void draw() {
		try {
			if (!PLAY_WITH_MOUSE) {
				context.update();
//				o.paint();
			}
			else {
				camera(mouseX, mouseY, (height/2) / tan(PI/6), width/2, height/2, 0, 0, 1, 0);
				
				if (keyPressed && key == CODED && keyCode == UP && !pressed2) {
					lvl.addPlayer(-1);
					pressed2 = true;
				}
				else if (!keyPressed) pressed2 = false;
			}
			
			background(255, 255, 255);
			for (GameObject o : lvl.objects) {
				o.update();
				o.display();
			}

			strokeWeight(8);
			stroke(0, 0, 0);
			line(Level.BORDER_LEFT, Level.BORDER_UP, Level.BORDER_RIGHT, Level.BORDER_UP);
			line(Level.BORDER_LEFT, Level.BORDER_UP, Level.BORDER_LEFT, Level.BORDER_DOWN);
			line(Level.BORDER_RIGHT, Level.BORDER_UP, Level.BORDER_RIGHT, Level.BORDER_DOWN);
			line(Level.BORDER_LEFT, Level.BORDER_DOWN, Level.BORDER_RIGHT, Level.BORDER_DOWN);
			
			line(0, 0, WIDTH, 0);
			line(0, 0, 0, HEIGHT);
			line(WIDTH, 0, WIDTH, HEIGHT);
			line(0, HEIGHT, WIDTH, HEIGHT);
			
			fill(0);
			textSize(14);
			text("Lvl: " + lvl.name + " [fps: " + (int) frameRate + "]", 10, 20);
			
			if (keyPressed && key == CODED && keyCode == RIGHT && !pressed) {
				// next lvl
//				Level oldlevel = lvl;
				lvl = Level.getNextLevel(this);
				for (int i : user) {
					lvl.addPlayer(i);
				}
				pressed = true;
			}
			else if (!keyPressed) pressed = false;
			
//			if (win) {
//				text("Win!", 10, 60);
//			}

		} catch (ConcurrentModificationException e) {
			// nope
		}
	}

//	private void drawVectors() {
//		Shape s = (Shape) lvl.objects.get(0);
//		
//		drawV(s, 2, 3, 0, 50, new PVector(-20,-40));
//		drawV(s, 1, 2, 50, 0, new PVector(-20, 40));
//		drawV(s, 0, 1, 0, -50, new PVector(-20, 40));
//		drawV(s, 3, 0, -50, 0, new PVector(20, -40));
//	}
	
//	private void drawV(Shape s, int i, int j, int x, int y, PVector vec) {
//	PVector wall = new PVector(s.shape.getVertex(i).x - s.shape.getVertex(j).x, 
//			s.shape.getVertex(i).y - s.shape.getVertex(j).y);
//	
//	
//	PVector vec2 = new PVector(vec.x, vec.y);
//	vec2.rotate(Shape.getRotateAngle(wall, vec));
//	//System.out.println(vec2.x+" "+vec2.y);
//	
//	PVector pos2 = new PVector(500+x, 350+y);
//	PVector pos = new PVector(pos2.x - vec.x, pos2.y - vec.y);
//	
//	stroke(0, 0, 255);
//	line(pos.x, pos.y, pos.x + vec.x, pos.y + vec.y);
//	stroke(255, 0, 0);
//	line(pos2.x, pos2.y, pos2.x + vec2.x, pos2.y + vec2.y);
//}
	
//	void win() {
//		win = true;
//	}
	
	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId) {
		println("onNewUser - userId: " + userId);
		context.startPoseDetection("Psi", userId);
	}

	public void onLostUser(int userId) {
		println("onLostUser - userId: " + userId);
		lvl.removePlayer(userId);
		user.remove(userId);
	}

	public void onStartCalibration(int userId) {
		println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull) {
		println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);

		if (successfull) {
			context.startTrackingSkeleton(userId);
			lvl.addPlayer(userId);
			user.add(userId);
		}
		else context.startPoseDetection("Psi", userId);
	}

	public void onStartPose(String pose, int userId) {
		println("onStartPose - userId: " + userId + ", pose: " + pose);
		context.stopPoseDetection(userId);
		context.requestCalibrationSkeleton(userId, true);
	}

	public void onEndPose(String pose, int userId) {
		println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

	class Output extends JFrame {	
		private static final long serialVersionUID = 1L;

		private BufferedImage image;
		private int[] imagePixels;
		private int w, h;
		
		public Output() {
			super(Playground.NAME+" - Kinect Screen");
			setSize(context.depthWidth(), context.depthHeight());
			setResizable(false);
			
			w = context.depthWidth();
			h = context.depthHeight();
			
			System.out.println(w+" "+h);
			
			image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
			imagePixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();		
			
			setVisible(true);
		}
		
		public void paint() {
			for (int y = 0; y < h; y++) {
				for (int x = 0; x < w; x++) {
					int index = (y * w) + x;
					imagePixels[index] = context.rgbImage().get(x, y);
				}
			}
			
			for (int i = 1; i < 10; i++) {
				if (context.isTrackingSkeleton(i)) {
					drawSkeleton(i);
					
					PVector pos = new PVector();
					context.getJointPositionSkeleton(i, SimpleOpenNI.SKEL_TORSO, pos);
					if (mousePressed) System.out.println(pos.x+" "+pos.y+" "+pos.z);					
				}
			}
			
			// draw border
//			drawBorder();
			
			getGraphics().drawImage(image, 0, 0, w, h, null);
		}
		
//		private void drawBorder() {
//			PVector frontLeft = new PVector(-FRONT_X, -400, FRONT_Z), frontLeftReal = new PVector();
//			PVector frontRight = new PVector(FRONT_X, -400, FRONT_Z), frontRightReal = new PVector();
//			PVector backLeft = new PVector(-BACK_X, 500, BACK_Z), backLeftReal = new PVector();
//			PVector backRight = new PVector(BACK_X, 500, BACK_Z), backRightReal = new PVector();
//			
//			context.convertRealWorldToProjective(frontLeft, frontLeftReal);
//			context.convertRealWorldToProjective(frontRight, frontRightReal);
//			context.convertRealWorldToProjective(backLeft, backLeftReal);
//			context.convertRealWorldToProjective(backRight, backRightReal);
//			
//			drawLine((int) frontLeftReal.x, (int) frontLeftReal.y, 
//					(int) frontRightReal.x, (int) frontRightReal.y, Color.WHITE.getRGB());
//			
//			drawLine((int) backLeftReal.x, (int) backLeftReal.y, 
//					(int) backRightReal.x, (int) backRightReal.y, Color.WHITE.getRGB());
//			
//			drawLine((int) frontLeftReal.x, (int) frontLeftReal.y, 
//					(int) backLeftReal.x, (int) backLeftReal.y, Color.WHITE.getRGB());
//			
//			drawLine((int) frontRightReal.x, (int) frontRightReal.y, 
//					(int) backRightReal.x, (int) backRightReal.y, Color.WHITE.getRGB());
//		}

		public void drawSkeleton(int userId){		  
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_HEAD, SimpleOpenNI.SKEL_NECK);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND, Color.GREEN);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND, Color.ORANGE);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_LEFT_HIP);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_RIGHT_HIP);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE);
		  drawSkeletonLine(userId, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT);  
		}
		
		public void drawSkeletonLine(int userId, int start, int end) {
			drawSkeletonLine(userId, start, end, Color.RED);
		}
		
		public void drawSkeletonLine(int userId, int start, int end, Color c) {
			PVector sPos = new PVector(), sPosReal = new PVector();
			PVector ePos = new PVector(), ePosReal = new PVector();

			context.getJointPositionSkeleton(userId, start, sPos);
			context.convertRealWorldToProjective(sPos, sPosReal);
			
			context.getJointPositionSkeleton(userId, end, ePos);
			context.convertRealWorldToProjective(ePos, ePosReal);
			
			drawLine((int) sPosReal.x, (int) sPosReal.y, (int) ePosReal.x, (int) ePosReal.y, c.getRGB());
		}
		
		public void drawLine(int srcX, int srcY, int destX, int destY, int col) {			
			// check input
			if (srcX < 0) srcX = 0;
			if (srcX >= width) srcX = width-1;
			if (destX < 0) destX = 0;
			if (destX >= width) destX = width-1;
			
			if (srcY < 0) srcY = 0;
			if (srcY >= height) srcY = height-1;
			if (destY < 0) destY = 0;
			if (destY >= height) destY = height-1;
			
			// Bresenham's line algorithm
			int dX = Math.abs(destX - srcX);
			int dY = Math.abs(destY - srcY);
			int sX = (srcX < destX ? 1 : -1);
			int sY = (srcY < destY ? 1 : -1);
			int err = dX - dY;
			int posX = srcX;
			int posY = srcY;
			
			while (true) {
				imagePixels[(posY * width) + posX] = col;
				if (posX == destX && posY == destY) break;
				
				int err2 = 2 * err;
				if (err2 > dY * -1) {
					err = err - dY;
					posX = posX + sX;
				}
				
				if (err2 < dX) {
					err = err + dX;
					posY = posY + sY;
				}
			}
		}
	}
}
