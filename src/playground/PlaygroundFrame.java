package playground;

import java.awt.BorderLayout;
//import java.awt.Dimension;
//import java.awt.GraphicsDevice;
//import java.awt.GraphicsEnvironment;
//import java.awt.Toolkit;

import javax.swing.JFrame;

import processing.core.PApplet;

public class PlaygroundFrame extends JFrame {
	private static final long serialVersionUID = 1L;
//	private static final int DEVICE = 0; // 0 = Laptop, 1 = Beamer

	public PlaygroundFrame() {
        super(Playground.NAME);

        setLayout(new BorderLayout());
        PApplet embed = new Playground();
        add(embed, BorderLayout.CENTER);
        
        setSize(Playground.WIDTH, Playground.HEIGHT);    
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setUndecorated(true);
        
//        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(1600, 0);
        
        setVisible(true);
        
        embed.init();
    }
	
	public static void main(String[] args) {
		new PlaygroundFrame();	
		
//		GraphicsDevice[] arr = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
//		arr[DEVICE].setFullScreenWindow(frame);
	}
}
