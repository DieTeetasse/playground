package playground;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import processing.core.PVector;

class Level {	
	enum Lvls {
//		LVL_TEST(),
		LVL_ALLSHAPES(),
		LVL_ONE(),
		LVL_TWO(),
		LVL_THREE(),
		LVL_FOUR(),
		LVL_FIVE();
	}
	
	static float BORDER_UP = 215;
	static float BORDER_DOWN = 705;
	static float BORDER_LEFT = 350;
	static float BORDER_RIGHT = 1076;
	
	static int curLvl = 5;
	
	String name;
	List<GameObject> objects;
	Color[] cPlayer;
	
	private Playground applet;
	private List<ParticleSystem> divs = new ArrayList<ParticleSystem>();
	private static Map<Lvls, Level> levels = new HashMap<Lvls, Level>();
	
	private Level(Playground applet, Lvls lvl) {
		this.applet = applet;
		initShapes();
		
		objects = new ArrayList<GameObject>();
		
		switch (lvl) {
			case LVL_ONE:
				name = "Level #01 - Einfarbig";
				cPlayer = new Color[] {Color.RED, Color.RED};
								
				addGoalShape(objects, 1, Color.RED);					
				break;
				
			case LVL_TWO:
				name = "Level #02 - Einfarbig mit Zuschauern";
				cPlayer = new Color[] {Color.RED, Color.RED};
				
				addGoalShape(objects, 1, Color.RED);
				addBlockerShape(objects, 0);
				addBlockerShape(objects, 2);
				addBlockerShape(objects, 10);
				break;
				
			case LVL_THREE:
				name = "Level #03 - Einfarbig mit Wechsler";
				cPlayer = new Color[] {Color.GREEN, Color.GREEN};
				
				addChangerShape(objects, 10, Color.RED);
				addGoalShape(objects, 4, Color.RED);
				break; 
				
			case LVL_FOUR:
				name = "Level #04 - Zweifarbig";
				cPlayer = new Color[] {Color.GREEN, Color.RED};
				
				addGoalShape(objects, 1, Color.GREEN);
				addGoalShape(objects, 5, Color.RED);
				break;
			
			case LVL_FIVE:
				name = "Level #05 - Die Slower";
				cPlayer = new Color[] {Color.RED, Color.RED};
				
				addSlowerShape(objects, 0, Color.BLACK);
				addGoalShape(objects, 5, Color.RED);
				break;
				
			case LVL_ALLSHAPES:
				name = "Testlevel #All Shapes";
				cPlayer = new Color[] {Color.RED, Color.RED};
				for (PointCloud p : shapes) objects.add(new ShapeBlocker(applet, p.x, p.y, Color.RED, p.vecs, p.goals));	
				break;
				
//			case LVL_TEST:
//			default:
//				name = "Testlevel #1337";
//				cPlayer = new Color[] {Color.GREEN, Color.RED};
//				
//				List<PVector> vecs = new ArrayList<PVector>();
//				vecs.add(new PVector(-50, -50));
//				vecs.add(new PVector(50, -50));
//				vecs.add(new PVector(50, 50));
//				vecs.add(new PVector(-50, 50));				
//				
//				List<int[]> goals = new ArrayList<int[]>();
//				goals.add(new int[]{2,3});
//			
//				objects.add(new Shape(applet, 500, 350, Color.GREEN, vecs, goals));
//				break;
		}
	}

	private void addSlowerShape(List<GameObject> objects, int index, Color col) {
		PointCloud c = shapes.get(index);
		objects.add(new ShapeSlower(applet, c.x, c.y, col, c.vecs));
	}
	
	private void addChangerShape(List<GameObject> objects, int index, Color col) {
		PointCloud c = shapes.get(index);
		objects.add(new ShapeColorChanger(applet, c.x, c.y, col, c.vecs));
	}
	
	private void addGoalShape(List<GameObject> objects, int index, Color col) {
		PointCloud c = shapes.get(index);
		objects.add(new ShapeBlocker(applet, c.x, c.y, col, c.vecs, c.goals));
	}
	
	private void addBlockerShape(List<GameObject> objects, int index) {
		PointCloud c = shapes.get(index);
		objects.add(new ShapeBlocker(applet, c.x, c.y, Color.BLACK, c.vecs, new ArrayList<int[]>()));
	}
	
	static Level getLevel(Playground applet, Lvls lvl) {
		if (levels.containsKey(lvl)) return levels.get(lvl);
		Level l = new Level(applet, lvl);
		levels.put(lvl, l);
		return l;
	}
	
	static Level getNextLevel(Playground applet) {
		Lvls lvl = Lvls.values()[curLvl];
		curLvl++;
		if (curLvl > Lvls.values().length - 1) curLvl = 0;
		
		if (levels.containsKey(lvl)) return levels.get(lvl);
		Level l = new Level(applet, lvl);
		levels.put(lvl, l);
		return l;
	}
	
	void addPlayer(int id) {
		addPlayer(id, 0, 0);
	}
	
	void addPlayer(int id, float x, float y) {
		ParticleSystem d = new ParticleSystem(applet, x, y, 2.5f, cPlayer, id);
		d.start();
		divs.add(d);
		objects.add(d);
	}
	
	void removePlayer(int id) {		
		for (ParticleSystem d : divs) {
			if (d.playerId == id) {
				divs.remove(d);
				objects.remove(d);
				break;
			}
		}	
	}
	
	// Mask Data
	private class Point {
		public int x;
		public int y;
		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
	private class PointCloud extends Point {
		public List<int[]> goals = new ArrayList<int[]>();
		public List<PVector> vecs = new ArrayList<PVector>();
		public PointCloud(int x, int y) {
			super(x, y);
			//System.out.println("shapes.add(new PointCloud("+(601 + ((x - 601) * 2))+", "+(395 + ((y - 395) * 2))+"));");
		}
		public void add(int x, int y) {
			vecs.add(new PVector(x, y));
			//System.out.println("shapes.get("+shapes.indexOf(this)+").add("+(int) (x * 2)+", "+(int) (y * 2)+");");
		}
		public void addGoal(int i, int j) {
			goals.add(new int[] {i, j});
		}
	}
	private LinkedList<PointCloud> shapes = new LinkedList<PointCloud>();
	
	private void initShapes() {
		shapes.add(new PointCloud(728, 492));
		shapes.add(new PointCloud(810, 404));
		shapes.add(new PointCloud(864, 480));
		shapes.add(new PointCloud(973, 477));
		shapes.add(new PointCloud(948, 607));
		
		shapes.add(new PointCloud(865, 616));
		shapes.add(new PointCloud(720, 598));
		shapes.add(new PointCloud(646, 574));
		shapes.add(new PointCloud(597, 608));
		shapes.add(new PointCloud(629, 460));
		
		shapes.add(new PointCloud(664, 353));
		shapes.add(new PointCloud(569, 352));
		shapes.add(new PointCloud(485, 317));
		shapes.add(new PointCloud(472, 374));
		shapes.add(new PointCloud(515, 463));
		
		shapes.get(0).add(-20, -41);
		shapes.get(0).add(20, -41);
		shapes.get(0).add(43, -17);
		shapes.get(0).add(43, 17);
		shapes.get(0).add(18, 41);
		shapes.get(0).add(-18, 39);
		shapes.get(0).add(-41, 18);
		shapes.get(0).add(-43, -14);

		shapes.get(1).add(-71, -22);
		shapes.get(1).add(71, -23);
		shapes.get(1).add(71, 21);
		shapes.get(1).add(-51, 23);
		shapes.get(1).add(-52, 5);
		shapes.get(1).add(-71, 3);

		shapes.get(2).add(-60, -29);
		shapes.get(2).add(1, -40);
		shapes.get(2).add(59, -28);
		shapes.get(2).add(60, 36);
		shapes.get(2).add(1, 28);
		shapes.get(2).add(-59, 40);

		shapes.get(3).add(-28, -30);
		shapes.get(3).add(-26, -11);
		shapes.get(3).add(-29, 31);
		shapes.get(3).add(27, 31);
		shapes.get(3).add(30, -10);
		shapes.get(3).add(26, -30);

		shapes.get(4).add(-101, -38);
		shapes.get(4).add(10, -37);
		shapes.get(4).add(6, -55);
		shapes.get(4).add(55, -48);
		shapes.get(4).add(102, 55);

		shapes.get(5).add(-57, -10);
		shapes.get(5).add(-54, 30);
		shapes.get(5).add(58, 14);
		shapes.get(5).add(-16, -30);
		shapes.get(5).add(-36, -30);

		shapes.get(6).add(-46, -38);
		shapes.get(6).add(-1, -52);
		shapes.get(6).add(47, -41);
		shapes.get(6).add(44, 52);
		shapes.get(6).add(0, 45);
		shapes.get(6).add(-43, 52);

		shapes.get(7).add(-35, -12);
		shapes.get(7).add(5, 25);
		shapes.get(7).add(36, -25);
		shapes.get(7).add(4, -8);

		shapes.get(8).add(-43, -53);
		shapes.get(8).add(-12, -38);
		shapes.get(8).add(-11, -15);
		shapes.get(8).add(25, -9);
		shapes.get(8).add(57, 25);
		shapes.get(8).add(1, 54);
		shapes.get(8).add(-57, 11);

		shapes.get(9).add(-35, -58);
		shapes.get(9).add(37, -16);
		shapes.get(9).add(36, 45);
		shapes.get(9).add(-36, 58);

		shapes.get(10).add(-57, 14);
		shapes.get(10).add(11, 48);
		shapes.get(10).add(57, -32);
		shapes.get(10).add(38, -41);
		shapes.get(10).add(-20, -48);

		shapes.get(11).add(-14, -38);
		shapes.get(11).add(41, 2);
		shapes.get(11).add(-9, 39);
		shapes.get(11).add(-40, -13);
		shapes.get(11).add(-28, -32);

		shapes.get(12).add(-5, -21);
		shapes.get(12).add(29, -21);
		shapes.get(12).add(18, 21);
		shapes.get(12).add(-26, 19);
		shapes.get(12).add(-28, -6);
		shapes.get(12).add(-13, -7);

		shapes.get(13).add(-35, -14);
		shapes.get(13).add(-8, -24);
		shapes.get(13).add(36, -10);
		shapes.get(13).add(34, 25);
		shapes.get(13).add(-7, 17);
		shapes.get(13).add(-36, 25);

		shapes.get(14).add(10, -63);
		shapes.get(14).add(9, 15);
		shapes.get(14).add(-52, 19);
		shapes.get(14).add(-55, 64);
		shapes.get(14).add(55, 63);
		shapes.get(14).add(55, -62);
		
		// "Goal"-Vertexes
		shapes.get(0).addGoal(0, 1);
		shapes.get(0).addGoal(1, 2);
		
		shapes.get(1).addGoal(3, 4);
		shapes.get(1).addGoal(4, 5);
		shapes.get(1).addGoal(5, 0);
		
		shapes.get(2).addGoal(3, 4);
		
		shapes.get(3).addGoal(4, 5);
		shapes.get(3).addGoal(5, 0);
		
		shapes.get(4).addGoal(0, 1);
		shapes.get(4).addGoal(1, 2);
		
		shapes.get(5).addGoal(3, 4);
		shapes.get(5).addGoal(4, 0);
		
		shapes.get(6).addGoal(0, 1);
		shapes.get(6).addGoal(1, 2);
		
		shapes.get(7).addGoal(0, 1);
		
		shapes.get(8).addGoal(1, 2);
		shapes.get(8).addGoal(2, 3);
		
		shapes.get(9).addGoal(1, 2);
		
		shapes.get(10).addGoal(3, 4);
		
		shapes.get(11).addGoal(0, 1);
//		shapes.get(11).addGoal(6, 0);
		
		shapes.get(12).addGoal(1, 2);
		shapes.get(12).addGoal(2, 3);
		
		shapes.get(13).addGoal(3, 4);
		
		shapes.get(14).addGoal(0, 1);
		shapes.get(14).addGoal(1, 2);
	}
	
	/*
	private void initShapes() {
		shapes.add(new PointCloud(463, 359));
		shapes.add(new PointCloud(601, 395));
		shapes.add(new PointCloud(709, 265));
		shapes.add(new PointCloud(771, 387));
		shapes.add(new PointCloud(891, 537));
		shapes.add(new PointCloud(779, 591));
		shapes.add(new PointCloud(321, 353));
		shapes.add(new PointCloud(503, 211));
		shapes.add(new PointCloud(397, 195));
		shapes.add(new PointCloud(925, 357));
		shapes.add(new PointCloud(273, 147));
		shapes.add(new PointCloud(441, 543));
		shapes.add(new PointCloud(263, 241));
		shapes.add(new PointCloud(483, 483));
		shapes.add(new PointCloud(615, 551));
		
		// Distance to edges from the middle
		shapes.get(0).add(-48, -80);
		shapes.get(0).add(48, -26);
		shapes.get(0).add(44, 56);
		shapes.get(0).add(-48, 80);
		shapes.get(1).add(-20, -56);
		shapes.get(1).add(28, -56);
		shapes.get(1).add(56, -22);
		shapes.get(1).add(56, 22);
		shapes.get(1).add(22, 52);
		shapes.get(1).add(-20, 56);
		shapes.get(1).add(-56, 22);
		shapes.get(1).add(-52, -22);
		shapes.get(2).add(-96, -26);
		shapes.get(2).add(96, -36);
		shapes.get(2).add(98, 28);
		shapes.get(2).add(-66, 36);
		shapes.get(2).add(-70, 12);
		shapes.get(2).add(-98, 4);
		shapes.get(3).add(-78, -44);
		shapes.get(3).add(72, -46);
		shapes.get(3).add(78, 42);
		shapes.get(3).add(-76, 46);
		shapes.get(4).add(-130, -42);
		shapes.get(4).add(14, -48);
		shapes.get(4).add(8, -72);
		shapes.get(4).add(74, -64);
		shapes.get(4).add(132, 74);
		shapes.get(5).add(-86, 64);
		shapes.get(5).add(88, -18);
		shapes.get(5).add(-24, -64);
		shapes.get(5).add(-54, -50);
		shapes.get(6).add(12, -88);
		shapes.get(6).add(72, -86);
		shapes.get(6).add(70, 84);
		shapes.get(6).add(-70, 88);
		shapes.get(6).add(-66, 28);
		shapes.get(6).add(10, 26);
		shapes.get(7).add(-74, 26);
		shapes.get(7).add(12, 68);
		shapes.get(7).add(74, -44);
		shapes.get(7).add(-26, -66);
		shapes.get(8).add(-20, 44);
		shapes.get(8).add(46, -8);
		shapes.get(8).add(-44, -44);
		shapes.get(9).add(-36, -40);
		shapes.get(9).add(38, -42);
		shapes.get(9).add(38, 38);
		shapes.get(9).add(-34, 42);
		shapes.get(10).add(-36, -4);
		shapes.get(10).add(-32, 32);
		shapes.get(10).add(24, 34);
		shapes.get(10).add(38, -34);
		shapes.get(10).add(-10, -26);
		shapes.get(10).add(-12, -4);
		shapes.get(11).add(-24, -44);
		shapes.get(11).add(-64, -72);
		shapes.get(11).add(-78, 16);
		shapes.get(11).add(2, 72);
		shapes.get(11).add(78, 32);
		shapes.get(11).add(28, -10);
		shapes.get(11).add(-22, -10);
		shapes.get(12).add(52, -30);
		shapes.get(12).add(50, 20);
		shapes.get(12).add(-48, 30);
		shapes.get(12).add(-52, -26);
		shapes.get(13).add(-40, 0);
		shapes.get(13).add(0, 30);
		shapes.get(13).add(40, 10);
		shapes.get(13).add(38, -30);
		shapes.get(13).add(10, -12);
		shapes.get(14).add(44, -62);
		shapes.get(14).add(42, 66);
		shapes.get(14).add(-44, 64);
		shapes.get(14).add(-44, -66);
		
		
		// "Goal"-Vertexes
		shapes.get(0).addGoal(0, 1);
		
		shapes.get(1).addGoal(0, 1);
		shapes.get(1).addGoal(1, 2);
		
		shapes.get(2).addGoal(3, 4);
		shapes.get(2).addGoal(4, 5);
		
		shapes.get(3).addGoal(3, 0);
		
		shapes.get(4).addGoal(2, 3);
		
		shapes.get(5).addGoal(1, 2);
		
		shapes.get(6).addGoal(4, 5);
		shapes.get(6).addGoal(5, 0);
		
		shapes.get(7).addGoal(0, 1);
		
		shapes.get(8).addGoal(1, 2);
		
		shapes.get(9).addGoal(1, 2);
		
		shapes.get(10).addGoal(4, 5);
		shapes.get(10).addGoal(5, 0);
		
		shapes.get(11).addGoal(5, 6);
		shapes.get(11).addGoal(6, 0);
		
		shapes.get(12).addGoal(0, 1);
		
		shapes.get(13).addGoal(3, 4);
		shapes.get(13).addGoal(4, 0);
		
		shapes.get(14).addGoal(0, 1);
	}	
	
	private void initShapes() {
		shapes.add(new PointCloud(532, 377));
		shapes.add(new PointCloud(601, 395));
		shapes.add(new PointCloud(655, 330));
		shapes.add(new PointCloud(686, 391));
		shapes.add(new PointCloud(746, 466));
		shapes.add(new PointCloud(690, 493));
		shapes.add(new PointCloud(461, 374));
		shapes.add(new PointCloud(552, 303));
		shapes.add(new PointCloud(499, 295));
		shapes.add(new PointCloud(763, 376));
		shapes.add(new PointCloud(437, 271));
		shapes.add(new PointCloud(521, 469));
		shapes.add(new PointCloud(432, 318));
		shapes.add(new PointCloud(542, 439));
		shapes.add(new PointCloud(608, 473));

		shapes.get(0).add(-24, -40);
		shapes.get(0).add(24, -13);
		shapes.get(0).add(22, 28);
		shapes.get(0).add(-24, 40);

		shapes.get(0).addGoal(0, 1);
		
		shapes.get(1).add(-10, -28);
		shapes.get(1).add(14, -28);
		shapes.get(1).add(28, -11);
		shapes.get(1).add(28, 11);
		shapes.get(1).add(11, 26);
		shapes.get(1).add(-10, 28);
		shapes.get(1).add(-28, 11);
		shapes.get(1).add(-26, -11);

		shapes.get(1).addGoal(0, 1);
		shapes.get(1).addGoal(1, 2);
		
		shapes.get(2).add(-48, -13);
		shapes.get(2).add(48, -18);
		shapes.get(2).add(49, 14);
		shapes.get(2).add(-33, 18);
		shapes.get(2).add(-35, 6);
		shapes.get(2).add(-49, 2);

		shapes.get(3).add(-39, -22);
		shapes.get(3).add(36, -23);
		shapes.get(3).add(39, 21);
		shapes.get(3).add(-38, 23);

		shapes.get(4).add(-65, -21);
		shapes.get(4).add(7, -24);
		shapes.get(4).add(4, -36);
		shapes.get(4).add(37, -32);
		shapes.get(4).add(66, 37);

		shapes.get(5).add(-43, 32);
		shapes.get(5).add(44, -9);
		shapes.get(5).add(-12, -32);
		shapes.get(5).add(-27, -25);

		shapes.get(6).add(6, -44);
		shapes.get(6).add(36, -43);
		shapes.get(6).add(35, 42);
		shapes.get(6).add(-35, 44);
		shapes.get(6).add(-33, 14);
		shapes.get(6).add(5, 13);

		shapes.get(7).add(-37, 13);
		shapes.get(7).add(6, 34);
		shapes.get(7).add(37, -22);
		shapes.get(7).add(-13, -33);

		shapes.get(8).add(-10, 22);
		shapes.get(8).add(23, -4);
		shapes.get(8).add(-22, -22);

		shapes.get(9).add(-18, -20);
		shapes.get(9).add(19, -21);
		shapes.get(9).add(19, 19);
		shapes.get(9).add(-17, 21);

		shapes.get(10).add(-18, -2);
		shapes.get(10).add(-16, 16);
		shapes.get(10).add(12, 17);
		shapes.get(10).add(19, -17);
		shapes.get(10).add(-5, -13);
		shapes.get(10).add(-6, -2);

		shapes.get(11).add(-12, -22);
		shapes.get(11).add(-32, -36);
		shapes.get(11).add(-39, 8);
		shapes.get(11).add(1, 36);
		shapes.get(11).add(39, 16);
		shapes.get(11).add(14, -5);
		shapes.get(11).add(-11, -5);

		shapes.get(12).add(26, -15);
		shapes.get(12).add(25, 10);
		shapes.get(12).add(-24, 15);
		shapes.get(12).add(-26, -13);

		shapes.get(13).add(-20, 0);
		shapes.get(13).add(0, 15);
		shapes.get(13).add(20, 5);
		shapes.get(13).add(19, -15);
		shapes.get(13).add(5, -6);

		shapes.get(14).add(22, -31);
		shapes.get(14).add(21, 33);
		shapes.get(14).add(-22, 32);
		shapes.get(14).add(-22, -33);
	}	
	*/
	
	public static void main(String[] args) {
		Level.getNextLevel(null);
	}
}
