package playground;

import java.util.List;

import processing.core.PVector;

public class ShapeColorChanger extends Shape {

	public ShapeColorChanger(Playground applet, float x, float y, Color c, List<PVector> vecs) {
		super(applet, x, y, c, c, vecs, TEXTURES.STREIFEN);
		this.shape.setFill(applet.color(c.r, c.g, c.b, 122));
	}
	
	@Override
	void collide(Particle p) {		
		// near?
		if (p.x < minX || p.x > maxX || p.y < minY || p.y > maxY) return;
		
		boolean inside = false;
		
		for (int i = 0; i < shape.getChild(0).getVertexCount(); i++) {
			int j = (i == 0 ? shape.getChild(0).getVertexCount() - 1 : i-1);
			PVector si = shape.getChild(0).getVertex(i);
			PVector sj = shape.getChild(0).getVertex(j);
			
			if ( ((si.y > p.y) != (sj.y > p.y)) &&
					(p.x < (sj.x - si.x) * (p.y - si.y) / (sj.y - si.y) + si.x) ) {
				inside = !inside;
			}
		}
		
		if (inside) {			
			p.c = c;
		}
	}
}
