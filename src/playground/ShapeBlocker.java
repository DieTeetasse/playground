package playground;

import java.util.List;

import processing.core.PVector;

public class ShapeBlocker extends Shape {
	List<int[]> goals;
	int particles;
	boolean complete = false;	

	public ShapeBlocker(Playground applet, float x, float y, Color c, List<PVector> vecs, List<int[]> goals) {
		super(applet, x, y, c, Color.BLACK, vecs, TEXTURES.KEINE);
		this.goals = goals;
	}
	
	@Override
	void collide(Particle p) {
		if (complete) return;
		
		// near?
		if (p.x < minX || p.x > maxX || p.y < minY || p.y > maxY) return;
		
		boolean inside = false;
		int[] border = null;
		float dist = Float.MAX_VALUE;
		PVector pv = new PVector(p.x, p.y);
		
		for (int i = 0; i < shape.getChild(0).getVertexCount(); i++) {
			int j = (i == 0 ? shape.getChild(0).getVertexCount() - 1 : i-1);
			PVector si = shape.getChild(0).getVertex(i);
			PVector sj = shape.getChild(0).getVertex(j);
			
			if ( ((si.y > p.y) != (sj.y > p.y)) &&
					(p.x < (sj.x - si.x) * (p.y - si.y) / (sj.y - si.y) + si.x) ) {
				inside = !inside;
			}
			
			// dist
			PVector s = new PVector(si.x + ((sj.x - si.x) / 2), si.y + ((sj.y - si.y) / 2));			
			float d = PVector.dist(s, pv);
			if (d < dist) {
				border = new int[]{j, i};
				dist = d;
			}
		}
		
		if (inside) {			
			for (int[] ii : goals) {
				if (ii[0] == border[0] && ii[1] == border[1] && c == p.c) {
					// GOAL!
					particles++;
					p.stop();
					return;
				}
			}
			
			// no goal => block
			PVector line = new PVector(shape.getChild(0).getVertex(border[0]).x - shape.getChild(0).getVertex(border[1]).x, 
					shape.getChild(0).getVertex(border[0]).y - shape.getChild(0).getVertex(border[1]).y);

			p.velocity.rotate(getRotateAngle(line, p.velocity));
		}
	}

	@Override
	void display() {	
		if (complete) return;
		
		applet.shape(shape);	
		
		for (int[] ii : goals) {
			applet.strokeWeight(3);
			applet.stroke(c.r, c.g, c.b);
			applet.line(shape.getChild(0).getVertex(ii[0]).x, shape.getChild(0).getVertex(ii[0]).y,
					shape.getChild(0).getVertex(ii[1]).x, shape.getChild(0).getVertex(ii[1]).y);
		}	
	}

	@Override
	void update() {
		int p = particles / 80;
		if (p > 255) {
			shape.setFill(applet.color(255, 255, 255));	//0, 0, 0
			complete = true;
			return;
		}
		particles--;
		shape.setFill(applet.color(c.r + (255 - p), c.g + (255 - p), c.b + (255 - p)));
//		shape.setFill(applet.color(c.r + p, c.g + p, c.b + p));
	}
}
