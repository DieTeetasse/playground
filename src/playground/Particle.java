package playground;

import processing.core.PApplet;
import processing.core.PShape;
import processing.core.PVector;

class Particle {	
	private static final int LIFE = Playground.PARTICLES / 3;
	PVector gravity = new PVector(0, 0.01f);
	
	PVector velocity;
	float lifespan = 255;
	PShape part;
	float partSize;

	float x, y;
	Color c;
	ParticleSystem ps;
	
	Particle(ParticleSystem ps) {
		this.ps = ps;
		
		partSize = 15; //random(10, 60);
		part = ps.applet.createShape();
		part.beginShape(PApplet.QUAD);
		part.noStroke();
		part.texture(ps.sprite);
//		part.normal(0, 0, 1);
		part.vertex(-partSize / 2, -partSize / 2, 0, 0);
		part.vertex(+partSize / 2, -partSize / 2, ps.sprite.width, 0);
		part.vertex(+partSize / 2, +partSize / 2, ps.sprite.width, ps.sprite.height);
		part.vertex(-partSize / 2, +partSize / 2, 0, ps.sprite.height);
		part.endShape();
	}

	PShape getShape() {
		return part;
	}

	void birth() {
		rebirth();
		
		x = -10000.0f;
		y = -10000.0f;
		part.translate(x, y);
		lifespan = ps.applet.random(LIFE - 1) * 1;
	}
	
	void rebirth() {
		this.c = ps.c;
		part.setTint(ps.applet.color(c.r, c.g, c.b, 255));
		
		velocity = new PVector(ps.a.x, ps.a.y);
		velocity.mult(ps.speed);
		lifespan = LIFE - 1;
		
		float ang = ps.applet.random(PApplet.TWO_PI);
		x = ps.x + (PApplet.sin(ang) * ps.applet.random(0.0f, ps.rad));
		y = ps.y + (PApplet.cos(ang) * ps.applet.random(0.0f, ps.rad));
		
		part.resetMatrix();
		part.translate(x, y);
	}

	boolean isDead() {
		if (lifespan < 0) {
			return true;
		} else {
			return false;
		}
	}

	public void update() {
//		if (lifespan < 0) {
//			lifespan++;
//			return;
//		}
		
		lifespan--;
		
		for (GameObject o : ps.applet.lvl.objects) {
//			if (PApplet.sqrt(PApplet.pow(x - o.x, 2) + PApplet.pow(y - o.y, 2)) <= o.rad) {
				o.collide(this);	
//			}
		}
		
		part.setTint(ps.applet.color(c.r, c.g, c.b, lifespan));
		
		velocity.add(gravity);
		x += velocity.x; 
		y += velocity.y;
		part.translate(velocity.x, velocity.y);
		
		if (isDead()) rebirth();
	}
	
	public void stop() {
		velocity = new PVector(0, 0);
	}
}
