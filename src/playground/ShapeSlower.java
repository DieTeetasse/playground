package playground;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import processing.core.PVector;

public class ShapeSlower extends Shape {
	Map<Particle, Long> particles = new HashMap<Particle, Long>();

	public ShapeSlower(Playground applet, float x, float y, Color c, List<PVector> vecs) {
		super(applet, x, y, c, c, vecs, TEXTURES.WELLEN);
	}
	
	@Override
	void collide(Particle p) {	
		// already collide?
		if (particles.containsKey(p) && (System.currentTimeMillis() - particles.get(p) < 2000))
			return;
		
		// near?
		if (p.x < minX || p.x > maxX || p.y < minY || p.y > maxY) return;
		
		boolean inside = false;
		for (int i = 0; i < shape.getChild(0).getVertexCount(); i++) {
			int j = (i == 0 ? shape.getChild(0).getVertexCount() - 1 : i-1);
			PVector si = shape.getChild(0).getVertex(i);
			PVector sj = shape.getChild(0).getVertex(j);
			
			if (((si.y > p.y) != (sj.y > p.y)) &&
					(p.x < (sj.x - si.x) * (p.y - si.y) / (sj.y - si.y) + si.x) ) {
				inside = !inside;
			}
		}
		
		if (inside) {			
			p.velocity.mult(0.25f);
			particles.put(p, System.currentTimeMillis());
		}
	}
}
