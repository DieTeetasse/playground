package playground;

abstract class GameObject {
	final Playground applet;
	float x, y, rad;
	
	public GameObject(Playground applet, float x, float y, float rad) {
		this.applet = applet;
		this.x = x; this.y = y; this.rad = rad;
	}
	
	abstract void collide(Particle p);
	abstract void display();
	abstract void update();
}
