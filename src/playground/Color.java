package playground;

enum Color {
	BLACK(0, 0, 0),
	GRAY(200, 200, 200),
	WHITE(255, 255, 255),
	YELLOW(255, 255, 0),
	ORANGE(255, 165, 0),
	BLUE(0, 0, 255),
	GREEN(0, 255, 0),
	RED(255, 0, 0),
	MAGENTA(255, 0, 255),
	CYAN(0, 255, 255);
	
	int r, g, b;		
	private Color(int r, int g, int b) {
		this.r = r; this.g = g; this.b = b;
	}
	
	public int getRGB() {
		return ((r << 16) | (g << 8) | b);
	}
}