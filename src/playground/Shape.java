package playground;

import java.util.List;

import processing.core.PApplet;
import processing.core.PShape;
import processing.core.PVector;

public abstract class Shape extends GameObject {	
	private static final int TEX_WIDTH = 240;
	private static final int TEX_HEIGHT = 144;
	
	static enum TEXTURES {
		KEINE("weiss.png"),
		WELLEN("wellen2.png"),
		STREIFEN("streifen.png");
		
		String path;
		private TEXTURES(String path) {
			this.path = path;
		}
	}
	
	PShape shape;
	float minX, minY, maxX, maxY;
	Color c;

	public Shape(Playground applet, float x, float y, Color c, Color border, List<PVector> vecs, TEXTURES texture) {
		super(applet, x, y, -1);
		this.c = c;
		getBounds(x, y, vecs);
		
		this.shape = applet.createShape(PShape.GROUP);
		
		// face
		PShape part = applet.createShape();
		part.beginShape();	
		part.stroke(border.r, border.g, border.b);
		part.strokeWeight(2);
		part.tint(c.r, c.g, c.b);
		part.texture(applet.loadImage("playground/res/"+texture.path));	
		
		for (PVector v : vecs) {
			part.vertex(x + v.x, y + v.y, 0,
					((x + v.x - minX) * (TEX_WIDTH / (maxX - minX))),
					((y + v.y - minY) * (TEX_HEIGHT / (maxY - minY))));
		}
		
		part.endShape(PApplet.CLOSE);
		this.shape.addChild(part);
		
		// sides
		for (int i = 0; i < vecs.size(); i++) {
			int j = (i == vecs.size()-1 ? 0 : i+1);
			
			part = applet.createShape();
			part.beginShape();
			part.stroke(border.r, border.g, border.b);
			part.strokeWeight(2);
			part.tint(Color.GRAY.r, Color.GRAY.g, Color.GRAY.b);
			part.texture(applet.loadImage("playground/res/"+TEXTURES.KEINE.path));	
			
			part.vertex(x + vecs.get(i).x, y + vecs.get(i).y, -50,
					((x + vecs.get(i).x - minX) * (TEX_WIDTH / (maxX - minX))),
					((y + vecs.get(i).y - minY) * (TEX_HEIGHT / (maxY - minY))));
			part.vertex(x + vecs.get(i).x, y + vecs.get(i).y, 0,
					((x + vecs.get(i).x - minX) * (TEX_WIDTH / (maxX - minX))),
					((y + vecs.get(i).y - minY) * (TEX_HEIGHT / (maxY - minY))));
			part.vertex(x + vecs.get(j).x, y + vecs.get(j).y, 0,
					((x + vecs.get(j).x - minX) * (TEX_WIDTH / (maxX - minX))),
					((y + vecs.get(j).y - minY) * (TEX_HEIGHT / (maxY - minY))));
			part.vertex(x + vecs.get(j).x, y + vecs.get(j).y, -50,
					((x + vecs.get(j).x - minX) * (TEX_WIDTH / (maxX - minX))),
					((y + vecs.get(j).y - minY) * (TEX_HEIGHT / (maxY - minY))));
			
			part.endShape(PApplet.CLOSE);
			this.shape.addChild(part);
		}
	}

	private void getBounds(float x, float y, List<PVector> vecs) {
		minX = maxX = x + vecs.get(0).x;
		minY = maxY = y + vecs.get(0).y;
		
		for (int i = 1; i < vecs.size(); i++) {
			if (minX > (x + vecs.get(i).x)) minX = (x + vecs.get(i).x);
			if (maxX < (x + vecs.get(i).x)) maxX = (x + vecs.get(i).x);
			if (minY > (y + vecs.get(i).y)) minY = (y + vecs.get(i).y);
			if (maxY < (y + vecs.get(i).y)) maxY = (y + vecs.get(i).y);
		}
	}
	
//	private void getBounds() {
//		minX = maxX = shape.getVertex(0).x;
//		minY = maxY = shape.getVertex(0).y;
//		
//		for (int i = 1; i < shape.getVertexCount(); i++) {
//			if (minX > shape.getVertex(i).x) minX = shape.getVertex(i).x;
//			if (maxX < shape.getVertex(i).x) maxX = shape.getVertex(i).x;
//			if (minY > shape.getVertex(i).y) minY = shape.getVertex(i).y;
//			if (maxY < shape.getVertex(i).y) maxY = shape.getVertex(i).y;
//		}
//	}

	@Override
	void collide(Particle p) {
	}

	@Override
	void display() {
		applet.shape(shape);	
	}

	@Override
	void update() {
	}
	
	static float getRotateAngle(PVector wall, PVector velocity) {
		PVector wall2 = new PVector(wall.x, wall.y);
		wall2.rotate(PApplet.PI / -2);
		
		float ang = PVector.angleBetween(wall, velocity);
		float a = (PApplet.PI / 2) - (ang - (PApplet.PI / 2));
		
		float rotate = 1.0f;
		if (PVector.angleBetween(wall2, velocity) < (PApplet.PI / 2)) rotate = -1.0f;
		
		return (a * 2) * rotate;
	}
}
