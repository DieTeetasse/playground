package playground;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import SimpleOpenNI.SimpleOpenNI;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PShape;
import processing.core.PVector;

class ParticleSystem extends GameObject {
	static final int HAND_LEFT = 0;
	static final int HAND_RIGHT = 1;
	static final int MAX_POS = 10;
	
	Playground applet;
	ArrayList<Particle> particles;
	PShape particleShape;
	PImage sprite;
	int n = Playground.PARTICLES;
	Color[] cArr = {Color.GREEN, Color.RED};
	PVector[] aArr = {new PVector(PApplet.sin(0), PApplet.cos(0)), new PVector(PApplet.sin(0), PApplet.cos(0))};
	Color c = cArr[HAND_LEFT];
	PVector a = aArr[HAND_LEFT];
	float speed;
	int playerId, runner = 0;
	Queue<float[]> lastPos = new LinkedBlockingQueue<float[]>(MAX_POS);
		
	ParticleSystem(Playground applet, float x, float y, float speed, Color[] cArr, int playerId) {
		super(applet, x, y, Playground.SPAWN_RAD);
		this.applet = applet;
		this.x = x; this.y = y; this.cArr = cArr;
		this.speed = speed; this.playerId = playerId;
		
		particles = new ArrayList<Particle>();
		particleShape = applet.createShape(PShape.GROUP);
		sprite = applet.loadImage("playground/res/streifen.png");
	}

	void start() {		
		// create
		for (int i = 0; i < n; i++) {
			Particle p = new Particle(this);
			particles.add(p);
			particleShape.addChild(p.getShape());
		}
		
		// spawn
		for (Particle p : particles) {
			p.birth();		
		}
	}

	void display() {
		applet.strokeWeight(2);
		applet.stroke(255, 0, 0);
		applet.fill(0, 0, 0, 0);
		applet.ellipse(x, y, rad*2, rad*2);
		
		applet.shape(particleShape);
	}
	
	void reset() {
		particles.clear();
		particleShape = applet.createShape(PShape.GROUP);
	}
	
	void update() {
		// runner
		runner++;
		if (runner == 2) runner = 0;
		c = cArr[runner];
		a = aArr[runner];
		
		// Particles
		for (Particle p : particles) p.update();
		
		if (!Playground.PLAY_WITH_MOUSE) {
			// spawn angle
			PVector lElbow = new PVector(), lHand = new PVector();
			applet.context.getJointPositionSkeleton(playerId, SimpleOpenNI.SKEL_LEFT_ELBOW, lElbow);
			applet.context.getJointPositionSkeleton(playerId, SimpleOpenNI.SKEL_LEFT_HAND, lHand);
			PVector rElbow = new PVector(), rHand = new PVector();
			applet.context.getJointPositionSkeleton(playerId, SimpleOpenNI.SKEL_RIGHT_ELBOW, rElbow);
			applet.context.getJointPositionSkeleton(playerId, SimpleOpenNI.SKEL_RIGHT_HAND, rHand);
		
			PVector leftArm = PVector.sub(lHand, lElbow);
			leftArm.z = 0.0f;
			PVector rightArm = PVector.sub(rHand, rElbow); 
			rightArm.z = 0.0f;
		
			// parrallele Arme?
			//float angle = PVector.angleBetween(leftArm, rightArm);
			//if (angle > 0.45f) return;		
			
			aArr[HAND_LEFT] = calcVelo(leftArm);
			aArr[HAND_RIGHT] = calcVelo(rightArm);
			
			// spawn pos
			PVector pos = new PVector();
			applet.context.getJointPositionSkeleton(playerId, SimpleOpenNI.SKEL_TORSO, pos);
			
//			System.out.println(pos.x+" | "+pos.y+" | "+pos.z);
			
			if (pos.z < Playground.FRONT_Z) return;
			if (pos.z > Playground.BACK_Z) return;
			
			float zPer = ((pos.z - Playground.FRONT_Z) / Playground.FRONT_BACK_DIST);
			float acc_x = Playground.FRONT_X + Playground.FRONT_BACK_DIST_X * zPer;
			
			if (pos.x > acc_x) return;
			if (pos.x < acc_x * -1) return;		
			
			float xx = Playground.WIDTH - Playground.WIDTH * ((pos.x + acc_x) / (2 * acc_x));
			float yy = Playground.HEIGHT * zPer;
			
			// check border
			if (xx > Level.BORDER_LEFT && xx < Level.BORDER_RIGHT && yy > Level.BORDER_UP && yy < Level.BORDER_DOWN) return;
			
			if (lastPos.size() == MAX_POS) lastPos.remove();
			lastPos.add(new float[] {xx, yy});
		
			float sumX = 0, sumY = 0;
			for (float[] p : lastPos) {
				sumX += p[0];
				sumY += p[1];
			}		
		
			// avg pos to smooth movement
			x = sumX / lastPos.size();
			y = sumY / lastPos.size();
		}
		else {
//			x = applet.mouseX;
//			y = applet.mouseY;
			
			if (applet.keyPressed && applet.key == 'w') y -= 10;
			if (applet.keyPressed && applet.key == 'a') x -= 10;
			if (applet.keyPressed && applet.key == 's') y += 10;
			if (applet.keyPressed && applet.key == 'd') x += 10;
			
//			if (applet.mouseButton == PApplet.LEFT) {
//				aArr[HAND_LEFT] = new PVector(aArr[HAND_LEFT].x + 0.1f, aArr[HAND_LEFT].y + 0.1f);
//			}
//			else if (applet.mouseButton == PApplet.RIGHT) {
//				aArr[HAND_RIGHT] = new PVector(aArr[HAND_RIGHT].x + 0.1f, aArr[HAND_RIGHT].y + 0.1f);
//			}
		}
	}

	private PVector calcVelo(PVector arm) {
		float aUp = PVector.angleBetween(arm, new PVector(0.0f, +1.0f, 0.0f));
		float aLeft = PVector.angleBetween(arm, new PVector(+1.0f,  0.0f, 0.0f));
		
		float angle = PApplet.PI - aUp;
		if (aLeft <= PApplet.PI / 2) angle = PApplet.PI + aUp;
		
		return new PVector(PApplet.sin(angle), PApplet.cos(angle));
	}
	
	@Override
	void collide(Particle p) {
		// nichts
	}
}
